// No.1
console.log("--------------NO.1--------------")
console.log("LOOPING PERTAMA");
var temp = 0;
while (temp < 20) {
    temp = temp + 2;
    console.log(temp +" - I Love Coding")
}

console.log("");
console.log("LOOPING KEDUA");
if (temp == 20) {
    while (temp > 0) {
        console.log(temp +" - I will become a mobile developer")
        temp = temp - 2;
    }
}

console.log("///////////////////////////////////////////");

// No.2
console.log("--------------NO.2--------------")
for (let param = 1; param <= 20; param++) {
    if (param % 2 == 0) {
        console.log(param + " - Berkualitas")
    } else if (param % 2 == 1) {
        if (param % 3 == 0) {
            console.log(param + " - I Love Coding")
        } else {
            console.log(param + " - Santai")
        }
    }
}
console.log("///////////////////////////////////////////");

//No.3
console.log("--------------NO.3--------------")
for (let x = 0; x < 4; x++) {
    for (let y = 0; y < 8; y++) {
        process.stdout.write("#")
    }
    console.log()
}
console.log("///////////////////////////////////////////");

//No.4
console.log("--------------NO.4--------------")
for (var x = 0; x < 7; x++) {
    for (var y = 0; y <= x; y++) {
        process.stdout.write("#")
    }
    console.log()
}
console.log("///////////////////////////////////////////");

//No.5
console.log("--------------NO.5--------------")
for (var x = 0; x < 8; x++) {
    for (var y = 0; y < 8; y++) {
        if ((x + y) % 2 == 0) {
            process.stdout.write(" ")
        } else {
            process.stdout.write("#")
        }
    }
    console.log()
}
console.log("///////////////////////////////////////////");