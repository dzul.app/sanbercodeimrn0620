//No.1
console.log("--------------NO.1--------------")
function arrayToObject(array) {
    
    var now = new Date();
    var thisYear = now.getFullYear();
    var age = "";
    for (let x = 0; x < array.length; x++) {
        for (let y = 0; y < array[x].length; y++) {
            if (array[x][3] > thisYear || array[x][3] == undefined) {
                age = "Invalid birth year";
            } else {
                age = thisYear - array[x][3]
            }
        }
        var tempObject = {}
        tempObject.firstName = array[x][0];
        tempObject.lastName = array[x][1]
        tempObject.gender = array[x][2]
        tempObject.age = age
        console.log((x + 1) + " ." + array[x][0] + " " + array[x][1], tempObject)
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

//No.2
console.log("--------------NO.2--------------")
function shoppingTime(memberId, money) {

    var products = [
        ["Sepatu brand Stacattu", 1500000], ["Sweater brand Uniklooh", 175000],["Baju brand Zoro", 500000], 
        ["Baju brand H&N", 250000], ["Casing Handphone", 50000],
    ];

    products = products.sort(function(a, b) {return b[1] - a[1]});

    var changeMoney = 0;
    var boughtProducts = [];
    var cart = {};


    if (memberId != '' && money != null) {
        if (money >= 50000) {
            changeMoney = money;
            for (let x = 0; x < products.length; x++) {
                if (changeMoney >= products[x][1]) {
                    changeMoney -= products[x][1]
                    boughtProducts.push(products[x][0])
                }
            }
    
            cart.memberId = memberId;
            cart.money = money;
            cart.listPurchased = boughtProducts;
            cart.changeMoney = changeMoney;
    
            return cart; 
        } else {
            return "Mohon maaf, uang tidak cukup";
        }
        
    } else {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

//No.3
console.log("--------------NO.3--------------")
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    rute = rute.sort(function(a,b) {return a < b ? -1 : 1})

    var output = [];
    var naik = 0;
    var tujuan = 0;
    var indexNaik = 0;
    var indexTujuan = 0;
    var totalPrice = 0;

    if (arrPenumpang !== undefined || arrPenumpang.length != 0) {
        for (let x = 0; x < arrPenumpang.length; x++) {
            naik = arrPenumpang[x][1];
            tujuan = arrPenumpang[x][2];
    
            for (let index = 0; index < rute.length; index++) {
                if (naik == rute[index]) {
                    indexNaik = index + 1;
                }
                if (tujuan == rute[index]) {
                    indexTujuan = index + 1;
                }
            }
            var listAngkot = {};
            totalPrice = (indexTujuan - indexNaik) * 2000;
            listAngkot.penumpang = arrPenumpang[x][0];
            listAngkot.naikDari = naik;
            listAngkot.tujuan = tujuan;
            listAngkot.bayar = totalPrice
            output.push(listAngkot);
        }
    }
    return output
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]))