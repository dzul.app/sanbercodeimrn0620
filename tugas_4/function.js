//No.1
console.log("--------------NO.1--------------")
console.log()
function teriak() {
    return "Hello Sanbers!"
}

console.log(teriak())
console.log()

//No.2
console.log("--------------NO.2--------------")
console.log()
function kalikan(param1, param2) {
    return param1 * param2;
}
var num1 = 12;
var num2 = 4;
console.log(kalikan(num1, num2))
console.log()

//No.3
console.log("--------------NO.3--------------")
console.log()
function introduce(name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + " dan saya punya hobby yaitu " + hobby;
}

var name = "Agus";
var age = "30";
var address = "Jl. Malioboro, Yogyakarta";
var hobby = "Gaming";

console.log(introduce(name, age, address, hobby))
console.log()