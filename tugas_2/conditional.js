// No.1
console.log("--------------NO.1--------------")
var name = "John";
var role = "";

if (name != "" && role == "") {
    console.log("Halo, " + name + ", Pilih peranmu untuk memulai game!");
} else if (name != "" && role != "") {
    console.log("Selamat datang di Dunia Werewolf, " + name);
    if (role.toLowerCase() == "penyihir") {
        console.log("Halo Penyihir " + name + ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (role.toLowerCase() == "guard") {
        console.log("Halo Guard " + name + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if (role.toLowerCase() == "werewolf") {
        console.log("Halo Werewolf " + name + ", kamu akan memakan mangsa setiap malam!");
    } else {
        console.log("Role atau peran tidak tersedia!!!");
    }
} else {
    console.log("Nama harus diisi!");
}
console.log("///////////////////////////////////////////");

// No.2
console.log("--------------NO.2--------------")
var day = 1;
var month = 8;
var year = 1945;
var conditionDay = day > 0 && day <= 31 ? day : false;
var conditionMonth = "";
var conditionYear = year >= 1900 && year <= 2200 ? year : false;
//Flag untuk console log output
var isExist = false;

switch (month) {
    case 1: conditionMonth = "Januari"; isExist = true; break;
    case 2: conditionMonth = "Februari"; isExist = true; break;
    case 3: conditionMonth = "Maret"; isExist = true; break;
    case 4: conditionMonth = "April"; isExist = true; break;
    case 5: conditionMonth = "Mei"; isExist = true; break;
    case 6: conditionMonth = "Juni"; isExist = true; break;
    case 7: conditionMonth = "Juli"; isExist = true; break;
    case 8: conditionMonth = "Agustus"; isExist = true; break;
    case 9: conditionMonth = "September"; isExist = true; break;
    case 10: conditionMonth = "Oktober"; isExist = true; break;
    case 11: conditionMonth = "November"; isExist = true; break;
    case 12: conditionMonth = "Desember"; isExist = true; break;
    default: console.log("Input bulan hanya boleh angka dan rangenya dari 1 - 12 "); isExist = false; break;
    
}

if(isExist) {
    if(conditionDay == false) {
        console.log("Input hari hanya boleh angka dan rangenya dari 1 - 31!");
    } else if (conditionYear == false) {
        console.log("Input tahun hanya boleh angka dan rangenya dari 1990 - 2200!");
    } else {
        console.log(conditionDay + " " + conditionMonth + " " + conditionYear)
    }
}

console.log("///////////////////////////////////////////");