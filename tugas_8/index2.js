var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let index = 0;
function letsRead(times) {
    if(index < books.length)
    readBooksPromise(times,books[index])
        .then(result => letsRead(result))
        .catch(error => console.log(error))
    index++;
}

letsRead(10000);
