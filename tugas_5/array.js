//No.1
console.log("--------------NO.1--------------")

function range(startNum, finishNum) {
    if (startNum != null && finishNum != null) {
        var flag = false;
        if (startNum < finishNum) {
            flag = true;
        }

        var array = [];

        if (flag == true) {
            var temp = finishNum - startNum;
            for (let index = 0; index <= temp; index++) {
                array[index] = startNum;
                startNum ++;
            }
        } else {
            var temp = startNum - finishNum;
            for (let index = 0; index <= temp; index++) {
                array[index] = startNum;
                startNum --;
            }
        }
        return array;
    } else {
        return -1;
    }
}
console.log(range(7, 14));

//No.2
console.log("--------------NO.2--------------")

function rangeWithStep(startNum, finishNum, step) {
    if (startNum != null && finishNum != null && step != null) {
        var flag = false;
        if (startNum < finishNum) {
            flag = true;
        }

        var array = [];

        if (flag == true) {
            var index = 0;
            while (startNum <= finishNum) {
                array[index] = startNum;
                startNum += step;
                index++;
            }
        } else {
            var index = 0;
            while (startNum >= finishNum) {
                array[index] = startNum;
                startNum -= step;
                index++;
            }
        }
        return array;
    } else {
        return -1;
    }
}

console.log(rangeWithStep(5, 50, 2));

//No.3
console.log("--------------NO.3--------------")

function sum(startNum, finishNum, step) {
    var caseSum;
    if (startNum != null && finishNum != null && step != null) {
        caseSum = 2;
    } else if(startNum != null && finishNum != null) {
        step = 1;
        caseSum = 1;
    } else if (startNum != null) {
        return 1;
    } else {
        return 0;
    }

    var array = [];
    var sum = 0;

    switch (caseSum) {
        case 1:
            array = range(startNum, finishNum);
            for (let index = 0; index < array.length; index++) {
                sum += array[index];
            }
            break;

        case 2:
            array = rangeWithStep(startNum, finishNum, step)
            for (let index = 0; index < array.length; index++) {
                sum += array[index];
            }
            break;
    
        default:
            break;
    }

    return sum;

}
console.log(sum(1, 10));

//No.4
console.log("--------------NO.4--------------")

function dataHandling(param) {

    for (let x = 0; x < param.length; x++) {
        for (let y = 0; y < param[x].length; y++) {
            if(y == 0) {
                console.log("Nomor ID: " + param[x][y])
            } else if(y == 1) {
                console.log("Nama Lengkap: " + param[x][y])
            } else if(y == 2) {
                console.log("TTL: " + param[x][y] + " " + param[x][y+1])
            } else if(y == 4) {
                console.log("Hobi: " + param[x][y])
            }
        }
        console.log()
    }

}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input);

//No.5
console.log("--------------NO.5--------------")

function balikKata(param) {

    var splitString = [];
    for (let index = 0; index < param.length; index++) {
        splitString[index] = param.charAt(index);
    }
    var paramLength = param.length;
    var reverseArray = [];
    var reverseString = "";
    for (let index = 0; index < paramLength; index++) {
        reverseArray[index] = splitString[paramLength - index - 1];
        reverseString += reverseArray[index];
    }
    return reverseString;
}
console.log(balikKata("SanberCode"))

//No.6
console.log("--------------NO.6--------------")

function dataHandling2(params) {
    var conditionMonth = "";
    params.splice(1, 1, "Roman Alamsyah Elsharawy");
    params.splice(2, 1, "Provinsi Bandar Lampung");
    params.splice(4, 0, "Pria");
    params.splice(5, 1, "SMA Internasional Metro");

    var date = params[3];
    var dateSplit = date.split("/");
    var intMonth = Number(dateSplit[1]);
    var dateJoin = dateSplit.join("-")
    dateSplit.sort(function(a, b){return b-a});
    var nameSlice = params[1].slice(0,15);

    switch (intMonth) {
        case 1: conditionMonth = "Januari"; break;
        case 2: conditionMonth = "Februari"; break;
        case 3: conditionMonth = "Maret"; break;
        case 4: conditionMonth = "April"; break;
        case 5: conditionMonth = "Mei"; break;
        case 6: conditionMonth = "Juni"; break;
        case 7: conditionMonth = "Juli"; break;
        case 8: conditionMonth = "Agustus"; break;
        case 9: conditionMonth = "September"; break;
        case 10: conditionMonth = "Oktober"; break;
        case 11: conditionMonth = "November"; break;
        case 12: conditionMonth = "Desember"; break;
    }

    console.log(params)
    console.log(conditionMonth)
    console.log(dateSplit)
    console.log(dateJoin)
    console.log(nameSlice)

}

var inputs = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(inputs);
